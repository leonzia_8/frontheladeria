/**
 * @license
 * Copyright 2019 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */

 import {LitElement, html, css} from 'lit';

 /**
  * An example element.
  *
  * @slot - This element has a slot
  * @csspart button - The button
  */
 export class MyElement extends LitElement {
   static get styles() {
     return css`
       :host {
         display: flow-root;
         border: solid 1px gray;
         padding: 16px;
         max-width: 100%;
         background-color:#B9DEF1;
       }
     `;
   }
 
   static get properties() {
     return {
 
     };
   }
 
   constructor() {
     super();
 
   }
 
   render() {
     return html`
       <slot></slot>
     `;
   }
 
  
 }
 
 window.customElements.define('my-element', MyElement);
 