import {LitElement, html, css} from 'lit';

export class MisCajeros extends LitElement {
  static get styles() {
    return css`
      #opciones {
        width: 100%;
        display: flex;
        justify-content: space-around;
      }
      #menu {
        display: flex;
        justify-content: space-evenly;
      }
      .numeroCaja {
        float: left;

      }
      #toppings {
        background-color: aqua;
        width:25%;
        margin-left: 47%;
        border: 1px solid;
        display: flex;
        justify-content: space-around;
      }
      #carrito{
        margin-right:2%;
        float: right;
        background-color:#22AAF0 ;  
      }
        #carrito:hover {
        box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24), 0 17px 50px 0 rgba(0,0,0,0.19);
        background-color: #4CAF50; 
        color: white;
      }
      #confirma{
        background-color:#22AAF0 ;

      }
      #confirma:hover {
        box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24), 0 17px 50px 0 rgba(0,0,0,0.19);
        background-color: #4CAF50; 
        color: white;
      }
    `;
  }

  static get properties() {
    return {
      tipo: {type: String},
      pedido: {type: Object},
      empleado: {type: Object},
      mensaje: {type: String},
      guardaCajero: {type: String},
      pedidosRestantesCaja1: {type: Number},
      pedidosRestantesCaja2: {type: Number},
      pedidosRestantesCaja3: {type: Number}
    };
  }


  constructor() {
    super();
    this.mensaje = false
    this.tipo = '';
    this.pedido = {
        id_empleado:0,
        cono:0,
        vaso_cuarto:0,
        pote_kilo:0,
        topping:0,
        fecha:new Date()
    }
  }

  render() {
    return html`
      <h4>Seleccione una caja, elija su pedido y agregue al carrito</h4>
      <div id="1" class="numeroCaja" @click=${this.pedidosRestantesCaja1 === "0" ? ()=>{alert("caja cerrada")} : this.elegircaja}  >caja N° 1 - Pedidos restantes: ${this.pedidosRestantesCaja1}</div><br/>
      <div id="2" class="numeroCaja" @click=${this.pedidosRestantesCaja2 === "0" ? ()=>{alert("caja cerrada")} : this.elegircaja}  >caja N° 2 - Pedidos restantes: ${this.pedidosRestantesCaja2}</div><br/>
      <div id="3" class="numeroCaja" @click=${this.pedidosRestantesCaja3 === "0" ? ()=>{alert("caja cerrada")} : this.elegircaja}  style="margin-bottom: 30px;">caja N° 3 - Pedidos restantes: ${this.pedidosRestantesCaja3}</div>

      <div id="opciones">
        <div>
          <div>
            Cono
            <input
              type="radio"
              @click=${this.elegirTipo}
              value="cono"
              name="tipo"
              class="tipo"
            />
            <p>2 gustos</p>
          </div>
        </div>
        <div>
          <div>
            Cuarto kilo
            <input
              type="radio"
              @click=${this.elegirTipo}
              value="cuarto"
              name="tipo"
              class="tipo"
            />
            <p>3 gustos</p>
          </div>
        </div>
        <div>
          <div>
            Kilo
            <input
              type="radio"
              @click=${this.elegirTipo}
              value="kilo"
              name="tipo"
              class="tipo"
            />
            <p>4 gustos</p>
          </div>
        </div>
      </div>
      ${this.tipo === 'cuarto' 
        ? html`<p>Topping:</p>
            <div id="toppings">
              <label>chocolate</label>
              <input type="radio" name="toping" value="chocolate" class="topping"/>
              <label>vainilla</label>
              <input type="radio" name="toping" value="vainilla" class="topping"/>
            </div>`
        : null}
        ${this.tipo === 'kilo' 
        ? html`<p>Topping:</p>
            <div id="toppings">
              <label>chocolate</label>
              <input type="radio" name="toping" value="chocolate" class="topping"/>
              <label>vainilla</label>
              <input type="radio" name="toping" value="vainilla" class="topping"/>
              <label>almendras</label>
              <input type="radio" name="toping" value="almendras" class="topping"/>
            </div>`
        : null}

        <button id="carrito" @click=${this.agregaItem}>Agregar al carrito</button>


        <div id="pedido">
          <p>Pedido:</p>
          <p>cono:${this.pedido.cono}</p>
          <p>vaso 1/4:${this.pedido.vaso_cuarto}</p>
          <p>Kilo:${this.pedido.pote_kilo}</p>
          <p>toppings:${this.pedido.topping}</p>
          <p>despachante asignado: ${this.empleado && this.empleado.nombre} ${this.empleado && this.empleado.apellido}
        </div>

        
        <button id="confirma" @click=${this.confirmaPedido}>Confirma pedido</button>
        <span>${this.mensaje ? "Item agregado" : null}</span >
      <div id="menu"></div>
    `;
  }


  connectedCallback() {
    super.connectedCallback();
    this.traeRepartidor()
    this.traeCajas()
}

async traeRepartidor() {
  
  do{
    const respuesta = await fetch(`http://localhost:8080/empleadoRandom`, {
            method: "get",
            headers: {
                "Content-Type": "application/json",
            },
        })
        .then(function(response) {
            return response.json();
        })
        .then(data => {
            this.empleado = data
            if(data.id){
              this.pedido.id_empleado = data.id
            }
        });
      }while(this.empleado.mensaje)
    

}

  async traeCajas(){

    await fetch(` http://localhost:8080/getAllCajeros`, {
      method: "get",
      headers: {
          "Content-Type": "application/json",
            },
        })
        .then(function(response) {
            return response.text();
        })
        .then(data => {
           var cajas = data.split(" ");
           this.pedidosRestantesCaja1 = cajas[0]
           this.pedidosRestantesCaja2 = cajas[1]
           this.pedidosRestantesCaja3 = cajas[2]

        });



  }

  elegirTipo(e) {
    this.tipo = e.target.value;
  }

  agregaItem(){
      var tipo = this.shadowRoot.querySelectorAll(".tipo")
      var topping = this.shadowRoot.querySelectorAll(".topping")

      for(var i = 0;i<tipo.length;i++){
        if(tipo[i].checked === true){
            if(i === 0){
                this.pedido.cono++
            }
            if(i === 1){
                this.pedido.vaso_cuarto++
            }
            if(i === 2){
                this.pedido.pote_kilo++
            }
   
        }
        tipo[i].checked = false  
      }

      for(var i = 0;i<topping.length;i++){
        if(topping[i].checked === true){
           this.pedido.topping++
          }
          topping[i].checked = false
        }

        this.mensaje = true
         this.mensaje = false

        
  }


  async confirmaPedido (){

    if(this.pedido.cono === 0 && this.pedido.vaso_cuarto === 0 && this.pedido.pote_kilo === 0){
      alert("debe ingresar un item !")
      return false
    }

    if(!this.guardaCajero){
      alert("debe elegir una caja")
      return false
    }

    console.log(this.pedido)

    await fetch("http://localhost:8080/savePedido", {
      method: "post",
      body: JSON.stringify(this.pedido),
      headers: {
          "Content-Type": "application/json",
      },
      })
      .then(function(response) {
          return response.json();
      })
      .then(data => { 
          console.log(data)

      });

    await fetch("http://localhost:8080/cajeros/" + this.guardaCajero , {
        method: "get",
        headers: {
            "Content-Type": "application/json",
        },
    })
    .then(function(response) {
        return response.json();
    })
    .then(data => {
      if(data === 10000){ 
        alert("Heladeria cerrada!!! En el menu de inicio, verá los informes y podrá reabrir la heladeria")       
      }
      
    });

    this.traeCajas()
    this.limpiar()
    this.traeRepartidor()

  }


   elegircaja(event){

    var caja1 = this.shadowRoot.getElementById("1")
    var caja2 = this.shadowRoot.getElementById("2")
    var caja3 = this.shadowRoot.getElementById("3")
    this.guardaCajero = event.target.id

     if(event.target.id === "1"){
       caja1.style.color = "blue"
       caja2.style.color = "black"
       caja3.style.color = "black"
     }
     if(event.target.id === "2"){
      caja1.style.color = "black"
      caja2.style.color = "blue"
      caja3.style.color = "black"
    }
    if(event.target.id === "3"){
      caja1.style.color = "black"
      caja2.style.color = "black"
      caja3.style.color = "blue"
    }
    
  }



  limpiar(){
    
    var caja1 = this.shadowRoot.getElementById("1")
    var caja2 = this.shadowRoot.getElementById("2")
    var caja3 = this.shadowRoot.getElementById("3")

    caja1.style.color = "black"
    caja2.style.color = "black"
    caja3.style.color = "black"
    this.guardaCajero = null
    this.tipo = "cono"
    this.pedido.id_empleado=0,
    this.pedido.cono=0,
    this.pedido.vaso_cuarto=0,
    this.pedido.pote_kilo=0,
    this.pedido.topping=0,
    this.pedido.fecha=new Date()
  }
}

window.customElements.define('mis-cajeros', MisCajeros);
