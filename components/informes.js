import { css, html, LitElement } from "lit";




export class Informes extends LitElement {

  static get properties() {
    return {
      nombre: { typo: String },
      apellido: { typo: String },
      dni: { type: Number },
      return: { typr: String },
      pedidos: { type: Object },
      ventasXEmpleado: { type: String }
    };
  }

  static get styles() {
    return css`
        #contenedor{
          display: flex;
          justify-content: space-around;
        }
        input{margin-bottom:1%;
            margin-left:3%;
        }
        button{width:90%;
          margin-left:3%;}
        hr {       
            width:90%;
            height: 3px;
            background-color: #1629d8;
            margin-top:1%;
            margin-bottom:1%;
            margin-left:3%;
            float:"left";
            }
        #form{
            // background-color:#8ed2e4;
            width: 200px
        }
        `;
  }

  constructor() {
    super();
    this.nombre=""
    

  }

  render() {

    return html`
          <div id="contenedor">
            <div id="form">
            
              <h3>Agregar empleado</h3>
              <input type="text" id="nombre" @input="${this.handleNombre}" name="nombre" placeholder="Nombre" required="required"/>
              <br/>
              <input type="text" id="apellido" @input="${this.handleNombre}" name="apellido" placeholder="Apellido" required/>
              <br/>
              <input type="number" id="dni" @input="${this.handleNombre}" name="dni" placeholder="Documento" required="required" />
              <br/>

              <hr />
              <button @click="${this.enviarDatos}" >Enviar Datos</button>
              <hr />
              <div>${this.return}</div>
            </div>

            <div>
              <h5>Informe de ventas</h5>
            <table BORDER class="default">
            <tr>
                <th>id</th>
                <th>Legajo empleado</th>
                <th>Cono</th>
                <th>Vaso 1/4</th>
                <th>Pote Kilo</th>
                <th>Topping</th>
                <th>Fecha</th>
            </tr>

            ${this.pedidos ? this.pedidos.map(x => {
      return html`
                    <tr>                  
                        <td>${x.id_pedidos}</td>
                        <td>${x.id_empleado}</td>
                        <td>${x.cono}</td>
                        <td>${x.vaso_cuarto}</td>
                        <td>${x.pote_kilo}</td>
                        <td>${x.topping}</td>
                        <td>${x.fecha}</td>
                    </tr>
                `
    }) : null}
            </table>
            </div>


            <div>
            <h5>Informe de ventas por legajo de empleados</h5>
                ${this.ventasXEmpleado}
            </div>


            </div>
        `
  }



  connectedCallback() {
    super.connectedCallback();
    this.traePedidos();

  }


  async traePedidos() {
    await fetch(`http://localhost:8080/getAllPedido`, {
      method: "get",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then(function (response) {
        return response.json();
      })
      .then(data => {
        this.pedidos = data
        var legajos = []
        for (var i = 0; i < this.pedidos.length; i++) {
          legajos.push(this.pedidos[i].id_empleado)
        }

        var ventas = {};

        legajos.forEach(function (numero) {
          ventas[numero] = (ventas[numero] || 0) + 1;
        });

        var texto = JSON.stringify(ventas).split('"')
        this.ventasXEmpleado = JSON.stringify(ventas)
        console.log(texto);

      });
  }



  handleNombre(event, tipo = event.target.name) {
    this[tipo] = event.target.value;
  }

  async enviarDatos() {


    console.log(this.apellido)

        
    if( !this.apellido  || this.apellido.length < 3){

      this.return = "datos incorrectos, ingrese nuevamente"
      return false
    }

    if(this.nombre.length < 3){

      this.return = "datos incorrectos, ingrese nuevamente"
      return false
    }
    if(this.dni.length <7 ||this.dni.length >8 ){

      this.return = "datos incorrectos,  ingrese nuevamente"
      return false
    }




    var data = {}
    data.nombre = this.nombre;
    data.apellido = this.apellido;
    data.dni = this.dni;



    await fetch("http://localhost:8080/saveEmpleado", {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then(function (response) {
        return response.json();
      })
      .then(usuario => {
        console.log(usuario)
        if (usuario.error) {
          this.return = "datos incorrectos, ingrese nuevamente"
        } else {
          this.return = "empleado agregado correctamente"

        }

      });

          this.shadowRoot.getElementById("nombre").value = "";
          this.shadowRoot.getElementById("apellido").value = "";
          this.shadowRoot.getElementById("dni").value = "";
          
      



  }


}


window.customElements.define('mis-informes', Informes);
