import {LitElement, html, css} from 'lit';

export class Inicio extends LitElement {
  static get styles() {
    return css`
      #titulo {
        display: flex;
        justify-content: center        
      }
      #menu {
        display: flex;
        justify-content: space-evenly;
      }
      #volver{
        position:absolute; /*El div será ubicado con relación a la pantalla*/
        left:10px; /*A la derecha deje un espacio de 0px*/
        right:0px; /*A la izquierda deje un espacio de 0px*/
        bottom:0px; /*Abajo deje un espacio de 0px*/
        height:50px; /*alto del div*/
      }
    `;
  }

  static get properties() {
    return {
      tipo: {type: String},
      contenedor: {type: String}

   };

  }

  constructor() {
    super();
    this.tipo = "volver"
  }

  render() {
    return html`
  
      ${this.tipo === "volver" ? 
     ( html`
         <div id="titulo">
        <h2>Seleccione entre las tres opciones de uso</h2>
        
      </div>
     <div id="menu">
        <h4 @click=${this._selectTipo}>Informes y Empleados</h4>
        <h4 @click=${this._abrirHeladeria}>Abrir heladeria</h4>
        <h4 @click=${this._selectTipo}>Cajas</h4>
      </div>`)
      :
      html `<div id="contenedor">
            ${this.contenedor}
            
            <h5 @click=${this._selectTipo} id="VOLVER">volver</h5>
          </div>`
      }
    `;
  }

  _selectTipo(e){
    this.tipo = e.target.textContent
    
    if(this.tipo === "Cajas"){
      this.contenedor = html`<mis-cajeros />`
    }
    if(this.tipo === "Informes y Empleados"){
      this.contenedor = html`<mis-informes>`
    }

  }


  async _abrirHeladeria(){
    await fetch(` http://localhost:8080/abrirHeladeria`, {
      method: "get",
      headers: {
          "Content-Type": "application/json",
            },
        })
        .then(function(response) {
            return response.text();
        })
        .then(data => {
          alert(data)

        });
  }





}

window.customElements.define('mi-inicio', Inicio);
